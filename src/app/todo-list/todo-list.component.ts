import { Component, OnInit } from '@angular/core';
import { Todo, TodoService } from '../services/todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  todos: Todo[] = [];

  constructor(private svcTodo:TodoService) {
    console.log("b: TodoListComponent.ctor()")
    console.log("e: TodoListComponent.ctor()")
  }

  ngOnInit(): void {
    console.log("b: TodoListComponent.ngOnInit()");
    this.svcTodo.findAll().subscribe( data => {
      this.todos = data;
    });
    console.log("e: TodoListComponent.ngOnInit()")
  }
}
