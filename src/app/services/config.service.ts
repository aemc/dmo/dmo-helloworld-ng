import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  public url: string = "";

  constructor() {
    console.log("b: ConfigService.ctor()");
    const protocol = window && window.location && window.location.protocol;
    console.log("-- protocol: " + protocol);
    const hostname = window && window.location && window.location.hostname;
    console.log("-- hostname: " + hostname);
    const port = window && window.location && window.location.port;
    console.log("-- port: " + port);
    this.url = protocol + "//" + hostname + ":" + port + "/api";
    console.log("-- url: " + this.url);
    console.log("e: ConfigService.ctor()");
  }
}
