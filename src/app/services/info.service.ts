import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ConfigService } from './config.service';

export interface HostInfo {
  hostname: string;
  ipAddress: string;
}

@Injectable({
  providedIn: 'root'
})
export class InfoService {

  constructor(private http: HttpClient, private configService: ConfigService) {
    // intentionally empty
  }

  findAll():Observable<HostInfo>{
    return this.http.get<HostInfo>(this.configService.url + "/info", {responseType: 'json'});
  }
}
