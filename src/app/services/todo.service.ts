import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';

export interface Todo {
  id:string|undefined;
  title: string;
  content: string;
}

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: HttpClient, private configService: ConfigService) {
    // intentionally empty
  }

  public findAll():Observable<Todo[]>{
    return this.http.get<Todo[]>(this.configService.url + "/todos", {responseType: 'json'});
  }

  public save(todo:Todo){
    return this.http.post<Todo>(this.configService.url + "/todos", todo);
  }
}
