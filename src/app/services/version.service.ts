import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ConfigService } from './config.service';

export interface VersionInfoData {
  version: string;
}

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  constructor(private http: HttpClient, private configService: ConfigService) {
    // intentionally empty
  }

  findAll():Observable<VersionInfoData>{
    return this.http.get<VersionInfoData>(this.configService.url + "/versionInfo", {responseType: 'json'});
  }
}
