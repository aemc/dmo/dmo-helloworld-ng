import { HostInfo } from './../services/info.service';
import { Component, OnInit } from '@angular/core';
import { InfoService } from '../services/info.service';

@Component({
  selector: 'app-info-page',
  templateUrl: './info-page.component.html',
  styleUrls: ['./info-page.component.css']
})
export class InfoPageComponent implements OnInit {

  hostInfo: HostInfo = {
    hostname: "unkown",
    ipAddress: "unknown"
  };

  constructor(private svcInfo:InfoService) {
    console.log("b: InfoComponent.ctor()")
    console.log("e: InfoComponent.ctor()")
  }

  ngOnInit(): void {
    console.log("b: InfoComponent.ngOnInit()");
    this.svcInfo.findAll().subscribe( data => {
      this.hostInfo = data;
    });
    console.log("e: TodoListComponent.ngOnInit()")
  }
}
