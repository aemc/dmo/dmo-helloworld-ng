import { VersionService } from './../services/version.service';
import { Component, OnInit } from '@angular/core';
import { VersionInfoData } from '../services/version.service';

@Component({
  selector: 'app-version-page',
  templateUrl: './version-page.component.html',
  styleUrls: ['./version-page.component.css']
})
export class VersionPageComponent implements OnInit {

  name: string = "VersionPageComponent";
  versionInfo: VersionInfoData = {
    version: "unknown"
  }

  constructor(private svcVersion: VersionService) {
    console.log("b: " + this.name + ".ctor()")
    console.log("e: " + this.name + ".ctor()")
  }

  ngOnInit(): void {
    console.log("b: " + this.name + ".ngOnInit()")
    this.svcVersion.findAll().subscribe( data => {
      this.versionInfo = data;
    });
    console.log("e: " + this.name + ".ngOnInit()")
  }

}
