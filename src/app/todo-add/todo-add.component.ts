import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Todo, TodoService } from '../services/todo.service';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css']
})
export class TodoAddComponent {

  todo: Todo;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private svcTodo: TodoService) {
    this.todo = { id: undefined, title: "", content: ""};
  }

  onSubmit(){
    this.svcTodo.save(this.todo).subscribe( result => this.gotoTodoList());
  }

  gotoTodoList(){
    this.router.navigate(['/todos']);
  }
}
