import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodoListComponent } from './todo-list/todo-list.component';
import { HomePageComponent } from './home-page/home-page.component';
import { InfoPageComponent } from './info-page/info-page.component';
import { VersionPageComponent } from './version-page/version-page.component';
import { TodoAddComponent } from './todo-add/todo-add.component';

const routes: Routes = [
  { path: '', component: HomePageComponent},
  { path: 'info', component: InfoPageComponent},
  { path: 'version', component: VersionPageComponent},
  { path: 'todos', component: TodoListComponent },
  { path: 'addtodo', component: TodoAddComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
